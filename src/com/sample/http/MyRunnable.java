package com.sample.http;



public class MyRunnable implements Runnable {
	private final String url;
	private final String context;

	MyRunnable(String url,String context) {
		this.url = url;
		this.context = context;
	}

	public void run() {

		String result = "";
		try {
			
			HttpClientHelper clientHelper = HttpClientHelper.getInstance();
			//Uncomment this line for testing
			clientHelper.executePostMethod(url, context, "application/xml", "UTF-8");
			//clientHelper.executeGetMethod(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

