package com.sample.http;


import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class HTTPMultiClient {
	private static final int MYTHREADS = 4;
	public static void main(String args[]) throws Exception {
		
		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		//Add the urls according to your need
		String[] hostList = {"",
				"",
				"",
		""};

		//All the request object in order 
		String[] request = {
				"",
				"",
				"",
				""
		};

		Date startDate = new Date();
		for (int i = 0; i < hostList.length; i++) {


			String url = hostList[i];
			String context  = request[i];
			//String status = getStatus(url,context);
			Runnable worker = new MyRunnable(url,context);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");

		Date endDate = new Date();

		System.out.println(endDate.getTime()-startDate.getTime()+ "ms");
	}

}

