package com.sample.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.util.EntityUtils;

public final class HttpClientHelper {

	private static HttpClientHelper instance;
	private String ticket ="" ;
	private ThreadSafeClientConnManager conMngr;

	private HttpClientHelper() {

	}

	public void init() {

		// Create an HttpClient with the ThreadSafeClientConnManager.
		// This connection manager must be used if more than one thread will
		// be using the HttpClient.
		conMngr = new ThreadSafeClientConnManager();
		conMngr.setMaxTotal(200);
	}

	public static HttpClientHelper getInstance() {

		if (instance == null) {
			instance = new HttpClientHelper();
			instance.init();
		}

		return instance;
	}

	public String executeGetMethod(String URL) {

		String data = null;

		try {
			HttpClient httpClient = new DefaultHttpClient(conMngr);
			HttpGet get = new HttpGet(URL);
			get.setHeader("ticket", ticket);
			HttpResponse response = httpClient.execute(get);
			HttpEntity entity = response.getEntity();
			data = EntityUtils.toString(entity);
			System.out.println("URL :"+URL+" Response Code : "+response.getStatusLine());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public String executePostMethod(String URL, String content, String mimeType, String charset) {
		String data = null;

		try {
			HttpClient httpClient = new DefaultHttpClient(conMngr);
			HttpPost post = new HttpPost(URL);
			post.setHeader("ticket", ticket);
			HttpEntity entity = new StringEntity(content, mimeType, charset);
			post.setEntity(entity);
			HttpResponse response = httpClient.execute(post);
			data = getContent(response.getEntity().getContent());
			System.out.println("URL :"+URL+" Response Code : "+response.getStatusLine());
			System.out.println("Data is :"+data);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	/**
	 * @param is the input stream
	 * @see java.io.InputStream
	 * @return the String content from input stream
	 */
	public static String getContent(InputStream is) {
			final char[] buffer = new char[0x10000];
			StringBuilder out = new StringBuilder();
			Reader in = new InputStreamReader(is);
			try {
			int read = 0;
			do {
				read = in.read(buffer, 0, buffer.length);
				if (read>0) {
				out.append(buffer, 0, read);
				}
			} while (read>=0);
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
			try {
					in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			}

			return out.toString();
	}

}